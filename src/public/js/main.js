//App entery point.

var map, geojsonData, geoJsonLayer;
let tts = {

    _addEventListeners() {

        //new file
        document.getElementById('geojsonUpload').addEventListener('change', function (event) {
            const file = event.target.files[0];
            if (file) {
                const reader = new FileReader();
                reader.readAsText(file, "UTF-8");
                reader.onload = async function (evt) {
                    try {

                        tts.geojsonData = JSON.parse(evt.target.result);
                        if (tts.geojsonData.type === 'FeatureCollection' || tts.geojsonData.type === 'Feature') {
                            tts.styleAndDisplay(file.name, tts.geojsonData).then(r => {

                            }).catch(e => {
                                bs.resNotify({
                                    success: false,
                                    msg: "failed to style and map"
                                })

                            });
                        } else {
                            bs.resNotify({success: false, msg: "Invalid GeoJSON file."})
                        }
                    } catch (e) {
                        console.error(e)

                        bs.resNotify({
                            success: false,
                            msg: "Error parsing json, is the file you uploaded proper geojson."
                        })
                        // alert('Error parsing GeoJSON');
                    }
                };
                reader.onerror = function (evt) {
                    bs.resNotify({success: false, msg: "Error reading file."})
                };
            }
        })
        //this then calls. styleAndDisplay

        // refine
        // Get the button element
        var submitButton = document.getElementById('submitFeedback');

        // Add a click event listener to the submit button
        submitButton.addEventListener('click', function () {
            // Get the textarea value
            let feedback = document.getElementById('feedbackText').value;


            tts.sendFeedback(feedback);
            // Example action: log the feedback to the console
            console.log('Feedback submitted:', feedback);

            // Optionally, you could clear the textarea after submitting
            document.getElementById('feedbackText').value = '';
        });


        //edit form


    },


    /**
     * This function receavs a new geojson and perfroms our 3 step proccess.
     * Gather info
     * Prompt LLM
     * Display preview.
     * ... allow feedback
     * @param filename
     * @param geojsonData
     * @return {Promise<void>}
     */
    styleAndDisplay(filename, geojsonData) {


        if (!map) {

            tts._createMap()
        }

        tts._createLayer(geojsonData)


        /// todo get first draft of styler


        displaySampleFeature(geojsonData)

        // let uf = extractUniquePropertyValues(geojsonData)

        const outputElement = document.getElementById('geojsonOutput');
        // outputElement.innerText += JSON.stringify(uf, null, 2)

        return getSimpleStyle(filename, outputElement.innerText).then(ret => {

            // renderResults(style);
            let style = ret.data;

            tts._updateStyle(style)

            //todo next get user feedback on this style.

            window.fss = style.model

            // console.log(model.getMessages())
        }).catch(e => {

            bs.resNotify({success: false, msg: "Failed to generate ai style sorry."})

            console.error(e);
        })


    },


    _createMap() {

        const map = L.map('map').setView([0, 0], 2); // Default to a global view
        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            maxZoom: 18,
            attribution: '© OpenStreetMap contributors'
        }).addTo(map);
        L.control.scale().addTo(map); // Adding scale control


        window.map = map;

        return map;
    },

    _createLayer(geojsonData) {
        const geoJsonLayer = L.geoJSON(geojsonData, {
            pointToLayer: function (feature, latlng) {
                return L.circleMarker(latlng, {});
            },

            onEachFeature: function (feature, layer) {
                if (feature.properties && feature.properties.popupContent) {
                    layer.bindPopup(feature.properties.popupContent);
                }
            }
        }).addTo(map);

        window.geoJsonLayer = geoJsonLayer;

        map.fitBounds(geoJsonLayer.getBounds());

        return geoJsonLayer;

    },

    _setOutput(msg) {

        let el = document.getElementById("geojsonOutput");

        el.innerText = typeof msg == "string" ? msg : JSON.stringify(msg, null, 2)

    },

    _updateStyle(style) {


        tts._msg = style; // Note i use msg here a s the text contents of the ai response.
        tts._llm = tts._msg.llm;  // in theat method i pass the Moessagemanager instance as llm for continued use in feedback.


        tts._setOutput(style)

        let f = convertFreeSimpleStyleToLeafletStyleFunction(style)


        geoJsonLayer.setStyle(f);
        let r = parseFloat(style.fill.radius)
        console.log("setting radius")
        geoJsonLayer.eachLayer(function (layer) {
            if (layer instanceof L.CircleMarker) {
                layer.setRadius(r); // Set new radius
            }
        });


        bs.resNotify({success: true, msg: "Applied the new style to your data."})
    },


    sendFeedback(str) {

        if (!tts._llm) {
            //no model
            bs.resNotify({
                success: false,
                msg: "Cant find active styling session to provide feedback on. Please upload a layer."
            })
            return;
        }


        tts._llm.addUserMsg(str);

        //this is the groq response
        let ret = tts._llm.getResponseRetryOnInvalid(tts._llm, validateAIFSS).then(ret => {

            let style = ret.data

            // let content = msg.content
            //
            // let json = JSON.parse(content);
            // json.llm = tts._llm; // continue the cicle for continued feedback
            //

            bs.resNotify({success: true, msg: style.remark})

            // let valid = validateAIFSS(json);

            // bs.resNotify(valid);

            tts._updateStyle(style)


        })

    }
}


function main() {
    console.log("Hello World")

    tts._addEventListeners()

}

main()