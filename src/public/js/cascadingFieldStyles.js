/**
 * cascadingFieldStyles
 * by syonfox
 *
 * style: accepts all leaflet path options and radius
 *    todo suport markers
 *
 *
 */


let abreviationMap = {
    style: "s",                         // style is as define in the prompt template style ts bellow.
    defaultStyle: "d",
    fieldName: "f",
    comparisonOperator: "op",
    valueStyleMap: "vsm",
    cascadingFieldStyle: "cfs",
    cascadingFieldOverride: "cfo",
    cascadingFieldOverrides: "cfos",
    applyFeatureStyle: "afs"
}
let myCfs = {}


/*let ssss = {
    d: {...style},
    afs: false, // dont look at geojson properties for a style note style is same as spec for vsm: style in feature.properties
    cfos: [
        {
            f: "foo",
            vsm: {
                val1: {raduis: 2},
                val2: {raduis: 4}
            }
        }, //... continue other rules
    ]
}*/
/*
let verbosifyMap = Object.fromEnteries(Object.enteries(s).map(e => [e[1], e[0]]));

let verbisify = (s) => {
    let o = {};
    Object.enteries(s).forEach(e => {
        o[verbosifyMap[e[0]]] = e[1];
    })

}
*/


let promptStyle = async (f, d) => {
    // prompt user to defint a vsm entry

    let tvsm = {
        "comparisonOperator": {
            "type": "select2",
            "data": ['=', '!=', '!==', '>', '>=', '<', '<=', 'contains', 'starts_with', 'ends_with']
        },
        "value": {
            "type": "input",
            "placeholder": "when properties[field]  <operater>  this value"
        },
        /*
        "what_styles_do_you_want": {
            "type": "checklist",
            "data": ['fill', 'stroke', 'advanced_stroke'],
            "value": ['fill', 'stroke', 'advanced_stroke']
        }*/
        "stroke": {"type": "input", "inputType": "checkbox"},
        "color": {"type": "input", "inputType": "color"},
        "weight": {"type": "input", "inputType": "number"},
        "opacity": {"type": "input", "inputType": "number"},


        "fill": {"type": "checkbox"},
        "fillColor": {"type": "input", "inputType": "color"},
        "fillOpacity": {"type": "input", "inputType": "range"},
        "fillRule": {
            "type": "radiolist",
            "data": ["nonzero", "evenodd"]
        },
        "radius": {
            "type": "input",
            "inputType": "color"
        },
        "dashArray": {"type": "input"},
        "dashOffset": {"type": "input"},
        "lineCap": {"type": "select", "data": ["butt", "round", "square"]},
        "lineJoin": {"type": "select", "data": ["round", "bevel", "miter"]},
    }

    return bs.prompt("Add Value Style Map Rule! Field=" + f, tvsm, d, {
            submit: "Procead to style Defonition",
            //prependHTML: `<p>THis rule applies to: <br>feature.properties.${f} OPERATOR (you value) <br> ... the defined style will be applied.</p> `
        }
    ).then(data => {



        // line_stroke : [corlor] opacity:|-------()-----| [weight]
        // shape_fill : [color] opacity [] []
        // circle_radius
        // > advanced
        // 		[dash_array datalist] [dash_offset num]
        //      [render sample line with svg ?]   ///https://developer.mozilla.org/en-US/docs/Web/SVG/Attribute/stroke-dasharray
        // 		[line cap] [line join] //todo sample images in labels

        let ts = {
            //"stroke": { "type": "input", "inputType": "checkbox"  },
            "color": {"type": "input", "inputType": "color"},
            "weight": {"type": "input", "inputType": "number"},
            "opacity": {"type": "input", "inputType": "number"},


            //"fill": { "type": "checkbox" },
            "fillColor": {"type": "input", "inputType": "color"},
            "fillOpacity": {"type": "input", "inputType": "range"},
            "fillRule": {
                "type": "radiolist",
                "data": ["nonzero", "evenodd"]
            },
            "radius": {
                "type": "input",
                "inputType": "color"
            },
            "dashArray": {"type": "input"},
            "dashOffset": {"type": "input"},
            "lineCap": {"type": "select", "data": ["butt", "round", "square"]},
            "lineJoin": {"type": "select", "data": ["round", "bevel", "miter"]},
        }
        return bs.prompt("Define Style Please:", ts, d, {
            prependHTML: `<p>This style will be applied if: <br> feature.properties.${f} ${data.comparisonOperator} '${data.value}'</p>`
        }).then(style => {

            style.comparisonOperator = data.comparisonOperator;
            let entry = [data.value, style];
            return entry;
        })


    })


}

// promptStyle("foo")






let styleAbreveationMap = {
    "radius": "r",              // Radius for point features (converted to circles)
    "stroke": "s",           // Whether to draw a stroke along the path
    "color": "sc",       // Stroke color
    "weight": "sw",              // Stroke width in pixels
    "opacity": "so",           // Stroke opacity
    "fill": "f",            // Whether to fill the path with color
    "fillColor": "fc",         // Fill color (defaults to the value of the color option)
    "fillOpacity": "fo",       // Fill opacity
    "fillRule": "fr",    // Defines how the inside of a shape is determined
    "comparisonOperator": "co", // Default comparison operator for field overrides
    "lineCap": "lc",       // Shape used at the end of the stroke
    "lineJoin": "lj",      // Shape used at the corners of the stroke
    "dashArray": "da",        // Stroke dash pattern
    "dashOffset": "do",       // Distance into the dash pattern to start the dash
    }

let cfoAbreveationMap = {
    "fieldName": "f",
    "valueStyleMap": "vsm"
}


let cfsAbreveationMap = {
    "defaultStyle": "ds",
    "cascadingFieldOverrides": "cfos",
    "applyFeatureStyle": "af",
    "useGlobalDefault": "ug"
}


// STRUCTURE
// cfs:
// 		ds: 
//			style
//		cfos: Array<cfo>
//			$item:
//				f:
//				vsm:
//					style

// let cfos = [cfo, cfo2, [field, [operator, value, style]]]


let cfs = {


    globalDefaultStyle: {
        "stroke": true,           // Whether to draw a stroke along the path
        "color": "#3388ff",       // Stroke color
        "weight": 3,              // Stroke width in pixels
        "opacity": 1.0,           // Stroke opacity
        "lineCap": "round",       // Shape used at the end of the stroke
        "lineJoin": "round",      // Shape used at the corners of the stroke
        "dashArray": null,        // Stroke dash pattern
        "dashOffset": null,       // Distance into the dash pattern to start the dash
        "fill": false,            // Whether to fill the path with color
        "fillColor": "*",         // Fill color (defaults to the value of the color option)
        "fillOpacity": 0.2,       // Fill opacity
        "fillRule": "nonzero",    // Defines how the inside of a shape is determined
        "radius": 6,              // Radius for point features (converted to circles)
        "comparisonOperator": "=" // Default comparison operator for field overrides
    },

    /**
     * MUST be object with valid
     * defaultStyle: style
     * cascadingFieldOverrides: Array<cfo>
     *
     * */
    validateCFS(cfsdef) {
        if (!cfsdef || typeof cfsdef !== 'object') {
            throw new Error('Invalid CFS definition. Must be an object.');
        }

        if (!cfsdef.defaultStyle || typeof cfsdef.defaultStyle !== 'object') {
            throw new Error('Invalid defaultStyle. Must be an object.');
        }
                                  //cascadingFieldOverrides
        if (!Array.isArray(cfsdef.cascadingFieldOverrides)) {
            throw new Error('Invalid cascadingFieldOverrides. Must be an array.');
        }

        cfs.validateStyle(cfsdef.defaultStyle)

        cfsdef.cascadingFieldOverrides.forEach(cfs.validateCFO);

        return true;
    },

    validateCFO(cfo) {
        if (!cfo || typeof cfo !== 'object') {
            throw new Error('Invalid cascadingFieldOverrides. Must be an object.');
        }

        if (!cfo.fieldName || typeof cfo.fieldName !== 'string') {
            throw new Error('Invalid fieldName. Must be a string.');
        }

        if (!cfo.valueStyleMap || typeof cfo.valueStyleMap !== 'object') {
            throw new Error('Invalid valueStyleMap. Must be an object.');
        }

        cfs.validateVSM(cfo.valueStyleMap);

        return true;
    },

    validateVSM(vsm) {
        if (!vsm || typeof vsm !== 'object') {
            throw new Error('Invalid valueStyleMap. Must be an object.');
        }

        Object.values(vsm).forEach(cfs.validateStyle);

        return true;
    },

    validateStyle(style) {
        if (!style || typeof style !== 'object') {
            throw new Error('Invalid style. Must be an object.');
        }

        // Additional style property validations can be added here

        return true;
    },

    computeStyle(feature, cfsDefinition) {
        let computedStyle = {...cfsDefinition.defaultStyle};

        cfsDefinition.cascadingFieldOverrides.forEach((cfo) => {
            const value = feature.properties[cfo.fieldName];

            if (cfo.valueStyleMap.hasOwnProperty(value)) {
                const fieldStyle = cfo.valueStyleMap[value];
                Object.assign(computedStyle, fieldStyle);
            }
        });

        return computedStyle;
    },
};


/**
 let cfs = {
 validateCFS(cfs), //required default, cfo|cascadingFieldOverrides array of valid
 validateCFO(cfo), // requires to be object with fieldName and valueStyleMap or vsm
 validateVSM(vsm), // required to be a object with values corasponding to a vlid style
 validtateStyle(style), //
 computeStyle()



 } ; // https://gitlab.com/vgeo1/plan-webmap/-/issues/302#note_1607595454

 */
cfs.compareValues = function (value, operation, otherValue) {
    switch (operation) {
        case '=':
        case '==':
        case '===':
            return value === otherValue;

        case '!=':
        case '!==':
            return value !== otherValue;

        case '>':
            return value > otherValue;

        case '>=':
            return value >= otherValue;

        case '<':
            return value < otherValue;

        case '<=':
            return value <= otherValue;

        case 'contains':
            if (typeof value === 'string' && typeof otherValue === 'string') {
                return value.includes(otherValue);
            }
            return false;

        case 'starts_with':
            if (typeof value === 'string' && typeof otherValue === 'string') {
                return value.startsWith(otherValue);
            }
            return false;

        case 'ends_with':
            if (typeof value === 'string' && typeof otherValue === 'string') {
                return value.endsWith(otherValue);
            }
            return false;

        default:
            throw new Error('Invalid operation');
    }
}
cfs.compareOperators = ['==', '===', '!=', '!==', '>', '>=', '<', '<=', 'contains', 'starts_with', 'ends_with'];


//cfs.// Assume compareValues function is available in the scope

/**
 * Returns a Leaflet style function based on the provided style definition.
 * @param {object} style - Style definition object.
 * @returns {function} - Leaflet style function.
 */


cfs.letLeafletStyleFunction = function (style) {
    try {
        cfs.validateCFS(style);

    } catch (e) {
        console.error(e);
        console.error("Invaled style defonition");
        return false;
    }


    //returns a function that applies the cascadingFieldStyle
    return function leafletStyle(feature) {

        // Initialize with the default style
        let out = {...style.default};

        out = Object.assign(out, cfs.globalDefaultStyle, style.defaultStyle)
        // ORDER OF EFFECT                       ^            ^
        // GLOBAL DEFAULT                       ^------------^
        // style.defaultStyle .... hapens here ^

        // style.cascadingFieldOverrides[0]
        // style.cascadingFieldOverrides[1] ...
        // feature.cascadingFieldOverrides[0] ...
        if ((style.applyFeatureStyle === "first" || style.applyFeatureStyle === "only")
            && feature.properties.style
            && typeof feature.properties.style === "object") {
            Object.assign(out, feature.properties.style);
        }

        if (style.applyFeatureStyle === "only") return out;

        style.cascadingFieldOverrides.forEach(override => {
            //for each override apply it

            if (typeof feature.properties[override.fieldName] === "undefiend")
                return; // skipit

            let value = feature.properties[override.fieldName]

            Object.entries(override.valueStyleMap).forEach(e => {

                let oValue = e[0]
                let oStyle = e[1]

                let op = oStyle.comparasonOperator || out.comparisonOperator || "=";
                if (cfs.compareValues(value, op, oValue)) {
                    Object.assign(out, oStyle)
                } // here we merger the stile if the
            })

            return out;
        })
    }


    /**
     * Accepts 2 formats
     *
     * the object based cascadingFieldStyle whitch is verbose json
     * and the comprests style fromat whitch is array based
     *
     * [[]]
     *
     *
     * */
    cfs.getLeafletStyleFunctionfunction = (style) => {


        /**
         * Leaflet style function.
         * @param {object} feature - GeoJSON feature.
         * @returns {object} - Leaflet style object.
         */
        return function leafletStyle(feature) {

            // Initialize with the default style
            let out = {...style.defaultStyle};

            // Apply cascadingFieldStyle overrides
            style.cascadingFieldOverrides.forEach(override => {
                let value = feature.properties[override.fieldName];

                // Check if the field value matches any in the valueStyleMap
                if (Object.keys(override.valueStyleMap).includes(value)) {
                    Object.assign(out, override.valueStyleMap[value]);
                } else {
                    // Handle comparison operators if specified
                    if (override.valueStyleMap.hasOwnProperty('comparisonOperator')) {
                        const operator = override.valueStyleMap.comparisonOperator;
                        if (operator && operator in compareOperators) {
                            const comparisonValue = override.valueStyleMap.value;
                            if (compareValues(value, operator, comparisonValue)) {
                                Object.assign(out, override.valueStyleMap);
                            }
                        }
                    }
                }
            });
            console.log("feature:style", out)
            return out;
        };
    }

// Example usage:
//onst leafletStyleFunction = getLeafletStyleFunction(style);
// Apply this function to a Leaflet layer or feature
// layer.setStyle(leafletStyleFunction);

// Example usage:
//console.log(compareValues(5, '>', 3)); // Output: true
//console.log(compareValues('hello world', 'contains', 'world')); // Output: true
//console.log(compareValues(true, '===', true)); // Output: true


// features of cfs
// declare a default style
// declare an array of cascading filed overrides. 

// optionsaly include a cfso in an propery for a special feature. 

// suports all leaflet path options 

// suports radius for points 


    class CFO {

        parseFOV(fov) {
        }

        parseStyle(style) {

            return
        }

        constructor(fieldName, comparasionOperator, comparasonValue, style) {


            //accepts compresed cfo as [fieldName,[]]


            // Format one CSF: [[field, operator, value], [raduis, ...]]
            if (Array.isArray) {
            }

            // fromat two Normal verbose {fieldName, comparasionOperator, comparasonValue, style}

            // optionaly define a fov for brevity, {fov: [], style: {}}

            //logic if array then CSF
            //else


            /**
             * Ok this is triky we have multiple desing constraints calshing
             *
             * i want this to be editable as json in whitch the value maps are seprate
             *
             * // cfo = {field, {value1: {style, operator} ...}}
             *
             * vs
             *
             * // cfo = [fov, style] here we need to define one override for each style
             *
             * // fo each cfo apply it
             * //vs for each cfo apply the value map
             *
             *
             *
             *
             *
             *
             * */
        }


    }

    // class FOV {
    //
    //     constructor(fovArray) {
    //         this.fovArray = fovArray; // Array of conditions
    //     }
    //
    //     // Evaluate if the conditions are met for a given feature
    //     evaluate(feature) {
    //         let isAndMode = this.fovArray.length % 3 == 0; // "AND" mode if more than one condition
    //
    //         for (let i = 0; i < this.fovArray.length; i += 3) {
    //             const field = this.fovArray[i];
    //             const operator = this.fovArray[i + 1];
    //             const value = this.fovArray[i + 2];
    //
    //             const conditionMet = this.applyCondition(feature[field], operator, value);
    //
    //             if (isAndMode && !conditionMet) {
    //                 return false; // In "AND" mode, if one condition fails, return false
    //             } else if (!isAndMode && conditionMet) {
    //                 return true; // In "OR" mode, if one condition is true, return true
    //             }
    //         }
    //
    //         return isAndMode; // For "AND" mode, return true if all conditions are met
    //     }
    //
    //     // Apply a single condition based on the operator
    //     applyCondition(featureValue, operator, conditionValue) {
    //
    //         return cfs.compareValues(featureValue, operator, conditionValue);
    //     }
    // }


    /**
     * csf = array of length n*3 or n*3 + 1
     * Represents a Field-Operator-Value (FOV) sequence for conditional styling in GIS applications.
     * This class allows for evaluating complex conditions based on feature attributes.
     */
    class FOV {

        /**
         * Constructs an FOV instance.
         *
         * @param {Array} fovArray - An array representing a sequence of conditions.
         * Each condition is a triplet [field, operator, value]. The array can contain
         * multiple such triplets. If the array has only one triplet, it is treated as "OR" logic.
         * If it contains multiple triplets, it is treated as "AND" logic.
         */
        constructor(fovArray) {

            console.assert(Array.isArray(fovArray));
            console.assert(fovArray.length >= 3);

            let remainder = fovArray.length % 3;

            // Here we pop the mode of the end if the remander is one; in case it was an non string we cast it to a string.

            if (remainder === 1) this.mode = (fovArray.pop() + "").toLowerCase(); // to lowercase for case insensitive modes like And == and == AND

            this.numFovs = (fovArray.length - remainder) / 3; // becuse we poped the last od digit we should have a multiple of 3 now!

            console.assert(fovArray.length % 3 === 0, "Make sure we didn get a sneky extra thing like 5 items only 4");

            this.fovArray = fovArray; // Array of conditions this satisfies the requirment of being a multipule of 3

            //return this
        }


        getFovs() {

            this.fovs = [];
            for (let i = 0; i < numFovs; i++) {
                let fov = i.splice(0, 3)
                fovs.push(fov);
            }

            return this.fovs;
        }

        /**
         * Evaluates the conditions against a given feature to determine if the associated style should be applied.
         * In "AND" mode, all conditions must be met. In "OR" mode, at least one condition must be met.
         *
         * @param {Object} feature - The feature object whose properties are evaluated against the FOV conditions.
         * @returns {boolean} True if the conditions are met, false otherwise.
         */
        evaluate(feature) {
            //todo more validation
            //let isAndMode = this.fovArray.length % 3 === 0; // "AND" mode if array length is a multiple of 3

            let isAndMode = true;

            if (this.mode) {
                if (!["^", "and", "&&"].includes(this.mode)) {
                    isAndMode = false; // Allow a mod of "AnD"
                    //["abc", 123].includes("aBc".toLowerCase()); // true
                } else if (!["v", "||", "or", false].includes(this.mode)) {
                    // wow special case here. this fov is an an unrecongnized mode
                    console.warn("Unrecognised Mode! must be and or  ")
                }
            }
            //ok essentaily if a mode is defeind we asume or; otherwise its and by default
            // it is possible to defein the mode as AND && or ^ depending on prefrence but i recomend just dont use it or append "or" when desired


            //ifthis.mode is defined then we defult to or
            // otherwise default is and
            // but if mode == "and" or ^ or && etc then its and mod as expected


            for (let i = 0; i < this.fovArray.length; i += 3) {
                const field = this.fovArray[i];
                const operator = this.fovArray[i + 1];
                const value = this.fovArray[i + 2];

                const conditionMet = this.applyCondition(feature[field], operator, value);

                if (isAndMode && !conditionMet) {
                    return false; // In "AND" mode, if one condition fails, return false
                } else if (!isAndMode && conditionMet) {
                    return true; // In "OR" mode, if one condition is true, return true
                }
            }

            return isAndMode; // For "AND" mode, return true if all conditions are met
        }

        /**
         * Applies a single condition based on the specified operator.
         * Delegates the comparison to an external 'compareValues' function from the 'cfs' object.
         *
         * @param {string} featureValue - The value of the feature's field to be evaluated.
         * @param {string} operator - The operator used for comparison ('==', '<', '>', etc.).
         * @param {string} conditionValue - The value to compare against the feature's field value.
         * @returns {boolean} The result of the comparison.
         */
        applyCondition(featureValue, operator, conditionValue) {
            return cfs.compareValues(featureValue, operator, conditionValue);
        }
    }


//csf = [].length = 10
    class Style {

        // Compressed style fromat is Array of length 10
        static fromCSF(styleCSF) {
            console.assert(styleCSF.length == 10, "CSF Style must be array of len 10!")
            return new Style(...styleCSF)
        }

        //2 modes style mode and icon mode
        static fromObject(so) {

            return new Style(so.raduis)
        }

        static fromIcon(i) {
            // 		var myIcon = L.icon({
            // this.iconUrl//: 'my-icon.png',
            // iconSize//: [38, 95],
            // iconAnchor//: [22, 94],
            // popupAnchor//: [-3, -76],
            // shadowUrl//: 'my-icon-shadow.png',
            let dashOffset = i.shadowSize//: [68, 95],
            let dashArray = i.shadowAnchor//: [22, 94]
            let lc = encodeURIComponent(i.iconUrl);
            let lj = encodeURIComponent(i.shadowUrl)
            let da = [i.iconSize, i.iconAnchor, i.popupAnchor, i.shadowSize, i.shadowAnchor]
                .map(e => e.join(" ")) // concat all the 'x y' pairs,
                .join(" "); // ie 0 1 2 3 4 5 6 7 8 9 10 = x1 y1 x2 y2 ... for all 5 points in a
            // 	//why not just jsonstringify an array or somthing .. or juet let it be an array.
            //
            //

            function decodeIcon(style) {


                let pointsArray = style.dashArray.split(" ");


                let reshapedArray = pointsArray.reduce((acc, val, index) => {
                        val = parseFloat(val); 		// A point must be 2 numbers
                        return (index % 2 === 0 ? 	// if even
                            acc.push([val]) : 		// add first cord if we are even 0 ,2, 4 etc
                            acc[acc.length - 1]        // else take last item
                                .push(val), acc)		// 	and add the second cord
                    }
                    , []); // start reduce with an empty array


                let [iconSize, iconAnchor, popupAnchor, shadowSize, shadowAnchor] = reshapedArray

                var myIcon = L.icon({
                    iconUrl: decodeURIComponent(style.lineCap),
                    iconSize,
                    iconAnchor,
                    popupAnchor,
                    shadowUrl: decodeURIComponent(style.lineJoin),
                    shadowSize,
                    shadowAnchor
                });

            }

        }


        /**
         * Options come from path
         * https://leafletjs.com/reference.html#path
         *
         * With raduis for circle
         * var myIcon = L.icon({
         iconUrl: 'my-icon.png',
         iconSize: [38, 95],
         iconAnchor: [22, 94],
         popupAnchor: [-3, -76],
         shadowUrl: 'my-icon-shadow.png',
         shadowSize: [68, 95],
         shadowAnchor: [22, 94]
         });
         *
         * */

        constructor(radius,  // for circil radius
                    opacity, color, // for fill
                    weight, strokeOpacity, strokeColor, // for stroke
                    lineCap, lineJoin, dashOffset, dashArray
        ) {


            if (Array.isArray(radius)) {
                //then interpert as CSF
            } else if (typeof radius == "object") {
                Object.assign(this, radius);

            } else {
                this.radius = radius;
                this.opacity = opacity;
                this.color = color;
                this.weight = weight;
                this.strokeOpacity = strokeOpacity;
                this.strokeColor = strokeColor;
                this.lineCap = lineCap;
                this.lineJoin = lineJoin;
                this.lineJoin = lineJoin;
                this.dashOffset = dashOffset;
            }


        }
    }


// csf = [default, cfos, options]
    class CFS {

        constructor(defaultStyle, cfos, options) {


            this.defaultStyle = new Style(defaultStyle);

            this.cascadingFieldOverrides = Array.isArray(cfos) ? cfos.map(cfo => new CFO(cfo)) : [];

            this.options = Object.assign({
                applyFeatureStyle: true, //"only" to skip cfs; fasle to igonre inline styles; "first" to apply inline stlyes before overrides; true or "last"/ defualt to apply these after you custom overrides.
                globalDefaultStyle: false, // use this to det aglobal defualt or true to use my default.

            }, options)


        }


        static fromCSF(csf) {
            return new CFS(csf[0], csf[1], csf[2]);
        }

        toCSF() {
            return [
                this.defaultStyle.toCSF(),
                this.cascadingFieldOverrides.map(c => c.toCSF()),
                this.options
            ]
        }

    }


//// Verbose format for CFS data
    let verboseCFSData = {
        defaultStyle: {
            radius: null,
            opacity: 0.5,
            color: "#ff7800",
            weight: 5,
            strokeOpacity: 0.8,
            strokeColor: "#000000",
            lineCap: "round",
            lineJoin: "miter",
            dashOffset: "0",
            dashArray: "5, 10"
        },
        cascadingFieldOverrides: [
            {
                fieldName: "road_class",
                comparisonOperator: "==",
                comparisonValue: "gravel",
                fov: [], // You MAY optionaly ommit the above in place of an fov array
                // OR you May omit this compleatly, if BOTH are fefined the specfied values are interperted FIRST
                // an FOV consists of some number n of field,operator,value in a singel 1d array.
                // an FOV MAY OPTIONALY conatin an last mode item whitch indicates
                //			secuential conditions are eveuleted with an OR between them
                // the fov array if defined must be multiple of n3 or n3+1, (l%3 ==0 || l%3==1 )

                style: {
                    radius: null,
                    opacity: 0.5,
                    color: "grey",
                    weight: 2,
                    strokeOpacity: 0.8,
                    strokeColor: "#000000",
                    lineCap: "round",
                    lineJoin: "miter",
                    dashOffset: "0",
                    dashArray: ""
                }
            },
            {
                fieldName: "road_class",
                comparisonOperator: "==",
                comparisonValue: "highway",
                style: {
                    radius: null,
                    opacity: 0.5,
                    color: "black",
                    weight: 10,
                    strokeOpacity: 0.8,
                    strokeColor: "#000000",
                    lineCap: "round",
                    lineJoin: "miter",
                    dashOffset: "0",
                    dashArray: "20, 10"
                }
            }
        ],
        options: {
            applyFeatureStyle: "last",
            globalDefaultStyle: true
        }
    }
}

// Load verbose format into CFS class
//let cfsInstanceVerbose = new CFS(verboseCFSData.defaultStyle, verboseCFSData.cfos, verboseCFSData.options);

// Use cfsInstanceVerbose in your application...





