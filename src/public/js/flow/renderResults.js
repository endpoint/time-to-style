function renderResults() {
    console.log(" Now we are initalizing the style function and testing it")


}

function convertFreeSimpleStyleToLeafletStyleFunction(freeSimpleStyle) {

    // Check if the input style object is valid
    const validation = validateAIFSS(freeSimpleStyle);
    if (!validation.success) {
        throw new Error(validation.msg);
    }

    return function(feature) {
        const style = {
            color: freeSimpleStyle.stroke.color,  // stroke color
            weight: freeSimpleStyle.stroke.width, // stroke width
            opacity: freeSimpleStyle.stroke.opacity, // stroke opacity
            fillColor: freeSimpleStyle.fill.color, // fill color
            fillOpacity: freeSimpleStyle.fill.opacity, // fill opacity
            radius:  freeSimpleStyle.fill.radius
        };

        // // Optional: Apply dynamic styling based on feature properties
        // if (freeSimpleStyle.label && feature.properties) {
        //     const labelField = freeSimpleStyle.label.field;
        //     if (feature.properties[labelField]) {
        //         // Example dynamic styling (could expand based on further rules)
        //         console.log("Label for this feature:", feature.properties[labelField]);
        //         // Implement additional style modifications if needed based on labelField
        //     }
        // }

        return style;
    };
}


// func