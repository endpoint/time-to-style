// let fetch = require("node-fetch")

class MessageManager {


    fetchAiResponseModelFactory(model, apiEndpoint, apiKey, performCompletion = (d) => {
        return d
    }) {

        let self = this;

        const fetchWithRetry = async (msgs, retryCount = 3) => {
            try {
                const response = await fetch(apiEndpoint, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${apiKey}`
                    },
                    body: JSON.stringify({
                        messages: msgs,
                        model: model,
                        temperature: 1,
                        max_tokens: 8192,
                        top_p: 1,
                        stream: false,
                        response_format: {type: "json_object"},
                        stop: null
                    })
                });

                if (!response.ok) {
                    let ret = {
                        success: false,
                        msg: `Network response was not ok. [${response.status}] ${response.statusText}`
                    }
                    bs.resNotify(ret);
                    // return ret;

                    throw new Error('Network response was not ok.' + response.status);
                }

                const data = await response.json();
                return performCompletion.call(self, data);

            } catch (error) {
                if (retryCount > 0) {
                    const delay = 500 + Math.random() * 500; // 500ms + random 0-500ms
                    console.error(`Attempt failed ${retryCount}, retrying in ${delay}ms.`, error);

                    bs.resNotify({
                        success: false,
                        msg: `Attempt failed ${retryCount}, retrying in ${delay}ms.`
                    })

                    await new Promise(resolve => setTimeout(resolve, delay));

                    return fetchWithRetry(msgs, retryCount - 1);

                }
                console.error('All retries failed:', error);
                throw error;
            }
        };

        return fetchWithRetry;
    }


    constructor(model, apiEndpoint, apiKey) {
        this.messages = [];


        this.model = model;
        this.apiEndpoint = apiEndpoint;
        this.apiKey = apiKey;


        this.fetch = this.fetchAiResponseModelFactory(model, apiEndpoint,
            apiKey, this.onResponse);
        console.log(this.fetch)
    }

    async fetchResponseRetry(msgs, retryCount = 3) {

        if(!msgs) msgs = this.getMessages();

        try {
            const response = await fetch(this.apiEndpoint, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${this.apiKey}`
                },
                body: JSON.stringify({
                    messages: msgs,
                    model: model,
                    temperature: 1,
                    max_tokens: 8192,
                    top_p: 1,
                    stream: false,
                    response_format: {type: "json_object"},
                    stop: null
                })
            });

            if (!response.ok) {
                let ret = {
                    success: false,
                    msg: `Network response was not ok. [${response.status}] ${response.statusText}`
                }
                bs.resNotify(ret);
                // return ret;

                throw new Error('Network response was not ok.' + response.status);
            }

            const data = await response.json();

            this.onResponse(data)
            // return /*performCompletion.call(self, data);*/
            return  {
                success: true,
                msg: "Successfully got response from AI",
                data
            }

        } catch (error) {
            //Retry by calling this function again
            if (retryCount > 0) {
                const delay = 500 + Math.random() * 500; // 500ms + random 0-500ms
                console.error(`Attempt failed ${retryCount}, retrying in ${delay}ms.`, error);

                let ret = {
                    success: false,
                    msg: `Attempt failed ${retryCount}, retrying in ${delay}ms.`
                }
                bs.resNotify(ret)

                await new Promise(resolve => setTimeout(resolve, delay));

                return this.fetchResponseRetry(msgs, retryCount - 1);

            }
            console.error('All retries failed:', error);

            let ret = {
                success: false,
                msg: `Attempt failed ${retryCount}, retrying in ${delay}ms.`
            }
            return ret;
            // throw error;
        }
    }


    async getResponse() {
        let now = Date.now()
        let ret = await this.fetchResponseRetry(this.getMessages());
        bs.resNotify(ret);
        console.log("got AI Response in : ", Date.now() - now, "ms")
        return ret;
    }

    onResponse(data) {
        let msg = data.choices[0].message
        this.addAiMsg(msg.content)
        console.log(msg)
        return data
    }

    addUserMsg(content) {
        this.messages.push({role: 'user', content});
    }

    addSystemMsg(content) {
        this.messages.push({role: 'system', content});
    }

    addAiMsg(content) {
        this.messages.push({role: 'assistant', content});
    }

    getMessages() {
        return this.messages;
    }

    getLastMsg() {
        return this.messages[this.messages.length - 1]
    }


    /**
     * This function allows you to perfrom the advanced logic of repeted queries
     * @param {MessageManager} myModel - the initialised chat to get the response of
     * @param {function} validation - a function that returns a standard ret object {success, msg}
     * @param {number} [tries=10]
     */
    async getResponseRetryOnInvalid(myModel, validation, tries = 10) {


        let ret = {success: false, msg: false}

        let msg = null;

        while (!ret.success && tries) {


            if (ret.msg) myModel.addUserMsg(ret.msg);

            let next = await myModel.getResponse()

            msg = JSON.parse(myModel.getLastMsg().content)

            ret = validation(msg)
            bs.resNotify(ret);
            tries--;
        }
        console.log("Last msg = ", msg)

        msg.llm = myModel;
        ret.data = msg;
        return ret;
    }


}


const apiEndpoint = 'https://api.groq.com/openai/v1/chat/completions';
const apiKey = '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'; // Replace with your actual API key
const model = 'llama3-8b-8192';
//
// const myModel = new MessageManager(model, apiEndpoint, apiKey);
//
// myModel.addSystemMsg("PLEASE RETURN JSON RET OBJECT ONLY: {success:bool, msg:string, data:any}")
// myModel.addUserMsg("Please return the structure of an ret object {succes, msg, data} for this error: body is undefined")


async function getSimpleStyle(file_name, sample_properties, extra) {
    const myModel = new MessageManager(model, apiEndpoint, apiKey);


    myModel.addSystemMsg("RETURN JSON IN FreeSimpleStyle FORMAT: { 'remark': 'string', 'fill': { 'color': 'string', 'opacity': 'number', 'radius': 'number' }, 'stroke': { 'color': 'string', 'width': 'number', 'opacity': 'number' } }. Use only valid JSON!");

    let initMsg = `GeoJSON data is available from the file: ${file_name}. We need to style this data using the FreeSimpleStyle format. Define the style using hexadecimal colors and choose an appropriate color based on the file name. Use the remark to say why this color is good.`;


    console.log("Generating FreeSimpleStyle: ", myModel.getMessages());

    myModel.addUserMsg(initMsg);


    let ret = myModel.getResponseRetryOnInvalid(myModel, validateAIFSS)


    console.log("Last msg = ", ret.data)

    return ret;

}

function validateAIFSS(style) {
    function isValidColor(color) {
        const hexColorRegex = /^#?([a-fA-F0-9]{6}|[a-fA-F0-9]{3})$/;
        const namedColor = ["black", "white", "red", "green", "blue", "yellow", "cyan", "magenta", "gray", "grey"];
        return hexColorRegex.test(color) || namedColor.includes(color.toLowerCase());
    }

    function isNumber(value) {
        return !isNaN(parseFloat(value)) && isFinite(value);
    }

    // Initialize the return object
    const result = {
        success: true,
        msg: "Style Validation Successful."
    };

    if (typeof style !== 'object' || style === null) {
        result.success = false;
        result.msg = "Style must be a non-null object.";
        return result;
    }

    if (typeof style.remark !== 'string') {
        result.success = false;
        result.msg = "Remark must be a string.";
        return result;
    }

    if (!style.fill || typeof style.fill !== 'object') {
        result.success = false;
        result.msg = "Fill must be an object with color, opacity, and radius.";
        return result;
    }
    if (typeof style.fill.color == "undefined" || !isValidColor(style.fill.color)) {
        result.success = false;
        result.msg = "Fill color is invalid.";
        return result;
    }
    if (!isNumber(style.fill.opacity) || style.fill.opacity < 0 || style.fill.opacity > 1) {
        result.success = false;
        result.msg = "Fill opacity must be a number between 0 and 1.";
        return result;
    }
    if (!isNumber(style.fill.radius)) {
        result.success = false;
        result.msg = "Fill radius must be a number.";
        return result;
    }

    if (!style.stroke || typeof style.stroke !== 'object') {
        result.success = false;
        result.msg = "Stroke must be an object with color, width, and opacity.";
        return result;
    }
    if (typeof style.stroke.color == "undefined" || !isValidColor(style.stroke.color)) {
        result.success = false;
        result.msg = "Stroke color is invalid.";
        return result;
    }
    if (!isNumber(style.stroke.width)) {
        result.success = false;
        result.msg = "Stroke width must be a number.";
        return result;
    }
    if (!isNumber(style.stroke.opacity) || style.stroke.opacity < 0 || style.stroke.opacity > 1) {
        result.success = false;
        result.msg = "Stroke opacity must be a number between 0 and 1.";
        return result;
    }
    //
    // if (!style.label || typeof style.label !== 'object') {
    //     result.success = false;
    //     result.msg = "Label must be an object with field, textColor, and fontSize.";
    //     return result;
    // }
    // if (typeof style.label.field !== 'string') {
    //     result.success = false;
    //     result.msg = "Label field must be a string.";
    //     return result;
    // }
    // if (typeof style.label.textColor == "undefined" || !isValidColor(style.label.textColor)) {
    //     result.success = false;
    //     result.msg = "Label text color is invalid.";
    //     return result;
    // }
    // if (!isNumber(style.label.fontSize)) {
    //     result.success = false;
    //     result.msg = "Label font size must be a number.";
    //     return result;
    // }

    return result;
}

// Example style object for testing
const exampleStyle = {
    remark: "This is a simple style example.",
    fill: {color: "#ff0000", opacity: 0.5, radius: 10},
    stroke: {color: "blue", width: 3, opacity: 0.8},
    label: {field: "name", textColor: "black", fontSize: 14}
};

console.log("Validation result:", validateAIFSS(exampleStyle));


// generator.next(); // Initialize the generator
//
// // Send a message
// generator.next('please create cfs');
