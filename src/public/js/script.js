// Selecting the elements
const main = document.querySelector('main');
const statusTop = document.querySelector('.status-top');
const messageContainer = document.querySelector('.msg-container');
const statusBottom = document.querySelector('.status-bottom');
const buttonBox = document.querySelector('.button-box');
const inputMsg = document.querySelector('.input-text');
const minHeight = 2;
const maxHeight = 11;
// Selecting all buttons and putting them in an array
const buttons = Array.from(document.querySelectorAll('.btn.custom-btn'));

// Defining each button individually with variable names
const btnSend = buttons.find(button => button.getAttribute('name') === 'send');
const btnVoice = buttons.find(button => button.getAttribute('name') === 'voice');
const btnUpload = buttons.find(button => button.getAttribute('name') === 'upload');
const btnRefine = buttons.find(button => button.getAttribute('name') === 'refine');
const btnRedo = buttons.find(button => button.getAttribute('name') === 'redo');
const btnUndo = buttons.find(button => button.getAttribute('name') === 'undo');
const btnSettings = buttons.find(button => button.getAttribute('name') === 'settings');


let path = location.pathname.split("/").slice(2).join('/')
//
let apiBase = "/chat/"+path
async function postChatApi(action, data, uri) {
   uri = uri || apiBase

    data = data || {};
    data.action = action;

    try {
        const response = await fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        });

        if (!response.ok) {
            // Handle HTTP error
            const errorData = await response.json();
            const errorMsg = errorData.msg || 'HTTP Error';
            return { success: false, msg: errorMsg, debug: errorData };
        }

        const jsonResponse = await response.json();
        return jsonResponse;
    } catch (error) {
        // Handle other errors (e.g., network issues)
        return { success: false, msg: 'An error occurred', debug: error };
    }
}



async function init() {
    let msg = await postChatApi("getMsgs", {limit: 10})
    // ideas : getMsgs getActor addMsg addActor delMsg delActor
    // on msg put actors respond in order.
    console.log(msg)
    // Sample flow with actors:  U[R,S,A] >repeat
    // User make question msg 1
    // resarch expands 2
    // sumarizer condenses 3
    // results derived 4
    // New question asked 5
    // new research 6 (-2 )


    // an idea for a higly paralelized mode
    /*
       Question
       Pleaswe list all relavent fields as an json array for the above
       for each domain create an actor to perfrom that indapendatly

       Question :
        how would a dynamic system perfrom
        aka ceo recruter

        //core team tecnician, entrapranure, manager
        //roles implemnt the actual solution
        //ensure the big picture vision is upheald and provide deep insights and structure to a project.
        // hire and fire and find the best personal for the job. as well as ensure all sop's and records are keaped and everything is running smoothly,

     */
    //Basic implmentation

     let actorR = await postChatApi("addActor", {
        name: "Researcher",
        prompt:"Please act as a domain expert and researcher. Your goal is to provide all known facts rules and structures in a consise maner that you know with high confedince based on your training data.",
        history: 2, // thenumberof mesages
    })
    let actorS = await postChatApi("addActor", {
        name: "Summarizer",
        prompt:"Please act as a domain expert and create exenlent high quality sumaries of the conversation provided."
        history: 3, // thenumberof mesages
    })

    let actorA = await postChatApi("addActor", {
        name: "ActionPlanner",
        prompt:"The user requested an action or request of some kind above. With your expandid information from your pears please deliver the clearest most productive answer possible! Include only relevant information and important facts.",
        history: 4, // thenumberof mesages U R S  is 3 but 4 allows previso ansowe context as well
    })




}


// Example: Adding a click event listener to one of the buttons
buttons[0].addEventListener('click', () => {
    // Your button click logic here
});

const msgInputPadding = 10*2;
const msgInputLineHeight = (inputMsg.clientHeight-msgInputPadding) / inputMsg.rows;

// Function to update textarea height based on content
function updateTextareaHeight(mainDom, textDom, minHeight, maxHeight) {

    const lineHeight = msgInputLineHeight;

    const numberOfLines = inputMsg.value.split("\n").length ; // so we always have a line to type
    console.log("NOL: ", numberOfLines)


    let numRows = Math.max(minHeight, // if VV is smaller then min use min
        Math.min(maxHeight, numberOfLines)) // needed lines bigger them max use max


    textDom.setAttribute("rows", ""); // end set rows

    textDom.style.height = numRows * lineHeight +20 + 'px'

    // Calculate the new height based on the number of lines
    // const newHeight = Math.min(maxHeight, Math.max(minHeight, numberOfLines * lineHeight));

    // Apply the new height with animation
    // textDom.style.transition = "height 0.2s linear"; // Adjust the duration for the desired animation speed
    // textDom.style.height = newHeight + "px";

    // Adjust main container height if needed
    // mainDom.style.height = "100%"; // Reset the main container height
    // if (numberOfLines >= maxHeight / lineHeight) {
    //     mainDom.style.overflowY = "scroll";
    //     mainDom.style.height = "70%"; // Set the main container height to 70% if the maximum is reached
    // }
}

// Example usage:
// const mainDom = document.querySelector('.main'); // Replace with your main container selector
// const textDom = document.querySelector('.input-text'); // Replace with your textarea selector
// const minHeight = 1; // Minimum number of rows
// const maxHeight = 10; // Maximum number of rows or any specific height you prefer

// Watch for input changes in the textarea
main.addEventListener('input', () => {
    updateTextareaHeight(main, inputMsg, minHeight, maxHeight);
});

// Call the function initially to set the initial height
updateTextareaHeight(main, inputMsg, minHeight, maxHeight);

