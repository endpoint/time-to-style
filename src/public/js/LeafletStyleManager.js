/**
 * Represents the style options for a Leaflet map feature.
 * This extends the standard path options with an additional radius option.
 * 
 * @typedef {Object} LeafletStyleSpec
 * @property {string} [color="#3388ff"] - The color of the path. Default is blue.
 * @property {number} [weight=3] - The width of the path in pixels. Default is 3.
 * @property {number} [opacity=1.0] - The opacity of the path. Default is 1.0.
 * @property {string} [lineCap="round"] - A string that defines shape to be used at the end of the stroke.
 * @property {string} [lineJoin="round"] - A string that defines shape to be used at the corners of the stroke.
 * @property {string} [dashArray=null] - A string that defines the dash pattern. Default is solid line.
 * @property {string} [dashOffset=null] - A string that defines the distance into the dash pattern to start the dash.
 * @property {string} [fillColor="#3388ff"] - The fill color of the path. Default is the same as color.
 * @property {number} [fillOpacity=0.2] - The fill opacity of the path. Default is 0.2.
 * @property {string} [fillRule="evenodd"] - A string that defines how the inside of a shape is determined.
 * @property {boolean} [interactive=true] - If false, the path will not emit mouse events and will act as a part of the underlying map.
 * @property {boolean} [smoothFactor=1.0] - How much to simplify the polyline on each zoom level. More means better performance and smoother look, and less means more accurate representation.
 * @property {boolean} [noClip=false] - Disable polyline clipping.
 * @property {number} [radius=10] - The radius of the circle marker. Default is 10.
 * 
 * 
 * todo define this in relation to freemap
 * 
 * 
 */
{
	  

/**
 * Class for managing styles of Leaflet map layers.
 */
class LeafletStyleManager {
  /**
   * Initializes a new instance of the LeafletStyleManager class.
   * @param {LeafletStyleSpec} defaultStyle - The default style for the layers.
   */
  constructor(defaultStyle) 
	  if(!Array.isArray(LeafletStyleManager._styles))LeafletStyleManager._styles = [];
	  
	  this.defaultStyle = defaultStyle || this.getDefaultStyle();
	  LeafletStyleManager.push(this);
	  this.initalizedLayers = [];
  }

  /**
   * Returns the default style.
   * @returns {LeafletStyleSpec} The default style specification.
   */
  getDefaultStyle() {
    return {
      color: '#3388ff',
      weight: 3,
      opacity: 1.0,
      lineCap: 'round',
      lineJoin: 'round',
      dashArray: null,
      dashOffset: null,
      fillColor: '#3388ff',
      fillOpacity: 0.2,
      fillRule: 'evenodd',
      interactive: true,
      smoothFactor: 1.0,
      noClip: false,
      radius: 10
    };
  }

  /**
   * Applies a style to a given layer.
   * @param {L.Layer} layer - The Leaflet layer to apply the style to.
   * @param {LeafletStyleSpec} style - The style to apply.
   */
  applyStyle(layer, style) {
    if (layer.setStyle) {
      layer.setStyle(style);
    }
  }

  /**
   * Initializes a layer with the default style or a provided style.
   * @param {L.Layer} layer - The Leaflet layer to initialize.
   * @param {LeafletStyleSpec} [style] - The style to apply. If not provided, the default style is used.
   */
  initializeLayer(layer, style) {
	this.initalizedLayers.push([layer, style])
    this.applyStyle(layer, style || this.defaultStyle);
  }
}

// Usage example
// const styleManager = new LeafletStyleManager();
// const layer = L.circleMarker([51.5, -0.09]);
// styleManager.initializeLayer(layer);
