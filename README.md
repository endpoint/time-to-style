# time-to-style

note api key available at 



## Getting started
Dependencies: endpoint-ei cloudflare-wrangler 
deploys to cloudflare pages

## Test and Deploy
```shell
git clone https://gitlab.com/endpoint/time-to-style.git

cd time-to-style
npm install   ;
npm run build ; #//endpoint-ei (ejs intermediate build  normal ran by cloudflare at build time)

# Use cat with a here-document to append the GROQ_API_KEY to .dev.vars
cat <<EOF >> .dev.vars
# Adding GROQ_API_KEY
GROQ_API_KEY=your_actual_api_key_here
EOF
npm run pages ; #// extra cloudflare runtime for local testing on 
firefox http://localhost:8111 
```

## Usage


## Support
Read the code pr reach out by creating an issue.  For freemap reach out to emma@endpoint.ca

## Roadmap
Day 1-2 planing
day 3-4 create data scraping and fetch request for groq
day 4-5 try tune prompt to generate free simple style
day 6-7 rest
day 8 polish and release/ document if time ;)
## Contributing
yes contribute make an issue

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
MIT please fork and share alike but alas

## Project status
8 day hackathon hope its good

Databricks Generative AI World Cup
Join us for a hackathon to showcase the best-quality Generative AI app for your specific industry
Edit project
Import from portfolio
Who can participate

    Ages 18+ only
    Specific countries/territories excluded

View full rules

3 days to deadline

View schedule

Deadline
May 6, 2024 @ 5:00pm PDT
Online

Public
$117,500 in prizes 	1137 participants
Databricks
Devpost icon rgb30px Managed by Devpost
Machine Learning/AI Databases Beginner Friendly

Welcome to the Generative AI World Cup by Databricks in collaboration with Amazon Web Services (AWS), where data scientists and AI/ML engineers team up to tackle real-world challenges with cutting-edge GenAI apps in their specific industry. Databricks will present industry-specific problems for participants to solve. Can you craft the most high-quality and innovative GenAI app that stands out in this competition, the one you can brag about? Build, iterate, refine your solutions, and transform your industry. Don’t miss your chance to shine in this thrilling competition and compete for a pool of over $100,000 in cash prizes!
Requirements
What to Build

Entrants must create a working Generative AI software application that fits into one of the Themes and uses Databricks. Projects should be in the following categories: 1) Healthcare & Life Science; 2) Financial Services; Retail & Consumer Goods; 3) Communications, Media & Entertainment; or 4) Manufacturing & Energy

Retail and Consumer Goods (RCG)


Merge customer, inventory and POS data to optimize the customer experience

In-store or online agents and employees at retailers can get a single view of the customer and inventory with recommendations to provide better real-time customer service, support, and guidance in-store or online. Empowering human agents with generative AI-enabled tools like chatbots or portals can provide a better customer experience, increase the share of wallet and increase customer lifetime value.

Financial Services


Analyze public and private data to make better investment decisions

Asset managers can leverage news analytics such as GDELT to better understand trends and geopolitical events globally that may affect their investment portfolios and decisions.

Healthcare and Life Sciences


Increase the efficiency of healthcare operations

The estimated potential savings from waste reduction in healthcare ranges anywhere from $191 billion to $286 billion. Medical coding and billing are highly manual, medical transcription costs are expensive, clinical documentation is burdensome, and with manual processing, there is a risk of medication errors. Help us address this inefficiency in healthcare that has a material impact on staff productivity, cost reduction and even on patients.

Manufacturing and Energy


Improve productivity of field service agents

Field service engineers are often challenged with accessing tons of documents that are intertwined and are long and complicated. Having an LLM or context-aware Q&A bot will significantly reduce the time required to diagnose the problem and will boost efficiencies. The challenge that manufacturers face is how to build and incorporate data from proprietary documents and internal knowledge bases into LLMs.

Communications, Media and Entertainment


Maximize audience experience

Generative AI chatbots enable customers to better self-serve, reducing the burden on human customer support agents. A similar generative AI chatbot can be internally facing, empowering human support agents with enhanced customer insights and the ability to query using natural language and increasing the throughput and efficacy of agents solving customer support queries.

What to Submit

    a NEWLY built Databricks Project that fits into one of the above themes 
    Access must be provided to a working Project for judging and testing by providing a link to a website, functioning demo, or a test build
    Text description - Please include the name of each teammate, your company email(s), country, job title, and what industry you are applying your submission for
    Demonstration video (around 5 minutes)

See Full Rules for more details. 

